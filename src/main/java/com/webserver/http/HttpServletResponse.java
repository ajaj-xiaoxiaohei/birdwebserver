package com.webserver.http;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HttpServletResponse {
    private Socket socket;
    private int statusCode = 200;
    private String statusReason = "OK";
    private File contentFile;

    // 响应头相关信息
    Map<String, String> headers = new HashMap<>();

//    private static MimeTypesFileTypeMap mime = new MimetypesFileTypeMap();

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public File getContentFile() {
        return contentFile;
    }

    public void setContentFile(File contentFile) {
        this.contentFile = contentFile;
//        addHeader("Content-Type: " + mime.getContentType());
    }

    private void addHeader(String name, String value){
        headers.put(name, value);
    }

    public HttpServletResponse(Socket socket) throws IOException {
        this.socket = socket;

        // 1、发送响应行
        responseLine("HTTP/1.1" + statusCode + " " + statusReason);

        // 2、发送响应头
        Set<Map.Entry<String,String>> entrySet = headers.entrySet();

        for(Map.Entry<String, String> e : entrySet){
            String key = e.getKey();
            String value = e.getValue();
            responseLine(key + ": " + value);

            // 单独发送回车+换行表示响应发送完毕
            responseLine("");
        }

        // 3、发送响应正文
        OutputStream out = socket.getOutputStream();
        if(contentFile !=  null){ // 若正文文件不为空
            FileInputStream fis = new FileInputStream(contentFile);
            byte[] data =new byte[1024 * 10];
            int len;
            while((len = fis.read(data)) != -1){
                out.write(data, 0, len);
            }
        }


    }

    private void responseLine(String line) throws IOException {
        OutputStream out = socket.getOutputStream();
        out.write(line.getBytes(StandardCharsets.ISO_8859_1));
        out.write(13);
        out.write(10);
    }
}
