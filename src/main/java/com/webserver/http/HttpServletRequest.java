package com.webserver.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class HttpServletRequest {
    private Socket socket;
    private String method; // 请求的方式
    private String uri; // 抽象地址
    private String protocol; // 协议

    // 抽象地址细分
    private String requestAddress; // 请求地址
    private String requestParameter; // 请求参数
    Map<String, String> parameters = new HashMap<>(); // 保存参数

    Map<String, String> headers = new HashMap<>(); // 保存消息头


    public HttpServletRequest(Socket socket) throws IOException {
        this.socket = socket;

            // 1、解析请求行
            String line = readLine();
            if(!line.isEmpty()){
                String[] data = line.split("\\s");
                method = data[0];
                uri = data[1];
                protocol = data[2];

                // 抽象地址细分 - 存在参数情况
                String[] uriData = uri.split("\\?", 2);
                requestAddress = uriData[0];
                if(uri.contains("?") && uriData[1].length() > 0){
                    requestParameter = uriData[1];
                    String[] parameterData = requestParameter.split("&");

                    for(String para : parameterData){
                        String[] s = para.split("=", 2);
                        String key = s[0];
                        String value = s[1];
                        parameters.put(key, value);
                    }
                }

            }

            // 2、解析消息头
            while(true){
                line = readLine();
                if(line.isEmpty()){
                    break;
                }

                String[] headerData = line.split(":\\s");
                String key = headerData[0];
                String value = headerData[1];
                headers.put(key, value);
            }


            // 3、解析消息正文
    }

    // 行读操作
    private String readLine() throws IOException {
        InputStream in = socket.getInputStream();

        int d;
        char pre = 'a', cur = 'a';
        StringBuilder builder = new StringBuilder();
        while ((d = in.read()) != -1) {
            char c = (char) d;

            if (pre == 13 && cur == 10) {
                break;
            }

            builder.append(c);
        }
        return builder.toString().trim();
    }
}
