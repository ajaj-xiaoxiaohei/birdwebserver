package com.webserver.core;

import com.webserver.http.HttpServletRequest;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ClientHandler implements Runnable {
    private Socket socket;

    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            // 1、接收请求
            HttpServletRequest request = new HttpServletRequest(socket);

            // 2、处理请求

            // 3、响应请求
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
